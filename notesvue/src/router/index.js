import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('../views/Dashboard.vue')
  },
  {
    path: '/notes',
    name: 'NoteList',
    component: () => import('../views/NoteList.vue')
  },
  {
    path: '/note/create',
    name: 'NoteEdit',
    component: () => import('../views/NoteEdit.vue')
  },
  {
    path: '/notes/:id',
    name: 'NoteList',
    component: () => import('../views/NoteList.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
