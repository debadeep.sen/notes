const Constants = {
    BASE_API_URL: 'http://localhost/kuiqnotes/notes/api/',
    // BASE_API_URL: 'https://debadeepsen.com/kuiq/api/',
    APP_USER: 'KUIQ_USER'
}

export default Constants;